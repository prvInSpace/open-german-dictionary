package cymru.prv.dictionary.german

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.common.DictionaryList
import cymru.prv.dictionary.common.Word
import cymru.prv.dictionary.common.WordType
import org.json.JSONObject
import java.util.function.BiFunction
import java.util.function.Function

class GermanDictionary(list: DictionaryList = DictionaryList()) : Dictionary(
    list,
    "de",
    mutableMapOf<WordType, Function<JSONObject, Word>>(
        WordType.noun to Function(::GermanNoun),
        WordType.verb to Function(::GermanVerb)
    )
) {
    @Override
    override fun getVersion(): String {
        return "1.2-beta-1"
    }
}