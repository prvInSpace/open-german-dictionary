package cymru.prv.dictionary.german.tenses

import cymru.prv.dictionary.german.GermanVerb
import org.json.JSONObject

class PreteriteTense(obj: JSONObject, verb: GermanVerb): GermanVerbTense(obj, verb) {

    override fun getDefaultSingularFirst(): MutableList<String> {
        return mutableListOf(stem + "te")
    }

    override fun getDefaultSingularSecond(): MutableList<String> {
        return mutableListOf(stem + "test")
    }

    override fun getDefaultSingularThird(): MutableList<String> {
        return mutableListOf(stem + "te")
    }

    override fun getDefaultPluralFirst(): MutableList<String> {
        return mutableListOf(stem + "ten")
    }

    override fun getDefaultPluralSecond(): MutableList<String> {
        return mutableListOf(stem + "tet")
    }

    override fun getDefaultPluralThird(): MutableList<String> {
        return mutableListOf(stem + "ten")
    }

}