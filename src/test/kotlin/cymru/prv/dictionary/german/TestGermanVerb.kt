package cymru.prv.dictionary.german

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.common.DictionaryList
import org.json.JSONObject
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestGermanVerb {

    @Test
    fun `stem for -en verbs should remove the -en suffix`(){
        val verb = GermanVerb(JSONObject().put("normalForm", "arbeiten"))
        assertEquals("arbeit", verb.stem)
    }

    @Test
    fun `future should default to the normalForm`(){
        val verb = GermanVerb(JSONObject().put("normalForm", "arbeiten"))
        assertEquals("arbeiten", verb.future)
    }

    @Test
    fun `should read overrides for tenses`(){
        val verb = GermanVerb(JSONObject()
            .put("normalForm", "arbeiten")
            .put("present", JSONObject()
                .put("stem", "test")
            )
        )

        assertEquals("test", verb.tenses.getValue("present").stem)
    }

}